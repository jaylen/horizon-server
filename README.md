# Horizon

A simple and solid application for automating builds with focus on:

* making development efficient
* reliability and security
* explicit and plain user interface


## Getting Started

### Prerequisites

* Java 9
* something else...

### Installing

Work in progress...

## Contibuting

Work in progress...

## License

This project is licensed under the GNU General Public License v3.0 - see the [LICENSE](LICENSE) file for details.
